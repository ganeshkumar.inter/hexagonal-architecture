package com.classpath.paybiz.account.application.port.out;

import com.classpath.paybiz.account.domain.Account;

public interface AccountLock {

	void lockAccount(Account.AccountId accountId);

	void releaseAccount(Account.AccountId accountId);

}
