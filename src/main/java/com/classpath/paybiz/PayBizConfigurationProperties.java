package com.classpath.paybiz;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "buckpal")
public class PayBizConfigurationProperties {

  private long transferThreshold = Long.MAX_VALUE;

}
